package io.pivotal.pipelinepracticum.flightstatus

import java.time.LocalDate

data class FlightUpdate(val flightNumber: String, val date: LocalDate, val status: String)