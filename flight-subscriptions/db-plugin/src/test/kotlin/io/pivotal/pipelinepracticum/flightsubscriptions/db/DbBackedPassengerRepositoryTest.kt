package io.pivotal.pipelinepracticum.flightsubscriptions.db

import io.pivotal.pipelinepracticum.flightsubscriptions.PassengerRepository
import io.pivotal.pipelinepracticum.flightsubscriptions.PassengerRepositoryContract
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner
import javax.sql.DataSource

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [DbBackedPassengerRepositoryTestConfig::class])
@ActiveProfiles("DbBackedPassengerRepositoryTest")
@DataJpaTest
@TestPropertySource(properties = [
    "spring.flyway.enabled=false",
    "spring.jpa.hibernate.ddl-auto=none"
])
class DbBackedPassengerRepositoryTest: PassengerRepositoryContract() {

    @Autowired
    private lateinit var dbBackedPassengerRepository: PassengerRepository

    @Autowired
    private lateinit var datasource: DataSource

    @Before
    fun setUpClass() {
        Migrator(datasource).migrate()
    }

    override fun repository(): PassengerRepository {

        return dbBackedPassengerRepository
    }
}

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories("io.pivotal.pipelinepracticum.flightsubscriptions.db")
@EntityScan("io.pivotal.pipelinepracticum.flightsubscriptions.db")
@Profile("DbBackedPassengerRepositoryTest")
class DbBackedPassengerRepositoryTestConfig {
    @Bean
    fun passengerRepository(passengerJpaRepository: PassengerJpaRepository): PassengerRepository {
        return DbBackedPassengerRepository(passengerJpaRepository)
    }
}
