CREATE TABLE flight_subscription (
	passenger_id BIGINT not null,
	flight_number varchar(255) not null,
	flight_date DATE
);

ALTER TABLE passenger MODIFY COLUMN subscriptions varchar(255) NULL