package io.pivotal.pipelinepracticum.flightsubscriptions.db

import org.flywaydb.core.Flyway
import javax.sql.DataSource

class Migrator(private val datasource: DataSource) {

    private val migrationChecks = mapOf(
            "1" to MigrationCheck1(),
            "2" to MigrationCheck2()
    )

    fun migrate() {
        pendingMigrations().forEach {
            migrationChecks[it]?.before()
            flyway(it).migrate()
            migrationChecks[it]?.after()
        }
    }

    private fun pendingMigrations() = flyway().info().pending().map { it.version.version }

    private fun flyway(targetVersion: String? = null) = Flyway
            .configure()
            .dataSource(datasource)
            .let { if(targetVersion != null) it.target(targetVersion) else it }
            .load()
}

interface MigrationCheck {
    fun before()
    fun after()
}

class MigrationCheck1 : MigrationCheck {
    override fun before() {
        println("BEFORE 1")
    }

    override fun after() {
        println("AFTER 1")
    }
}

class MigrationCheck2 : MigrationCheck {
    override fun before() {
        println("BEFORE 2")
    }

    override fun after() {
        println("AFTER 2")
    }
}