package io.pivotal.pipelinepracticum.flightsubscriptions.db

import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import io.pivotal.pipelinepracticum.flightsubscriptions.Passenger
import io.pivotal.pipelinepracticum.flightsubscriptions.PassengerRepository
import org.springframework.data.repository.CrudRepository
import java.sql.Date

interface PassengerJpaRepository: CrudRepository<PassengerJpaEntity, Long>

class DbBackedPassengerRepository(private val jpaRepository: PassengerJpaRepository) : PassengerRepository {
    override fun savePassenger(passenger: Passenger) = domainModelFor(jpaRepository.save(entityFor(passenger)))

    override fun findPassengerById(id: String) = jpaRepository
            .findById(id.toLong())
            .map(::domainModelFor)
            .orElse(null)

    override fun findAllPassengers() = jpaRepository.findAll().map(::domainModelFor)

    private fun entityFor(domainModel: Passenger): PassengerJpaEntity {
        val entity = PassengerJpaEntity(
                id = domainModel.id?.toLong(),
                name = domainModel.name,
                subscriptions = emptyList()
        )

        val subscriptions = domainModel.subscriptions.map { FlightSubscriptionJpaEntity(
                flightNumber = it.flightNumber,
                flightDate = Date.valueOf(it.date),
                passenger = entity
        ) }

        entity.subscriptions = subscriptions

        return entity
    }

    private fun domainModelFor(entity: PassengerJpaEntity) = Passenger(
            id = entity.id?.toString(),
            name = entity.name,
            subscriptions = entity.subscriptions.map { Flight(it.flightNumber, it.flightDate.toLocalDate()) }
    )

}
