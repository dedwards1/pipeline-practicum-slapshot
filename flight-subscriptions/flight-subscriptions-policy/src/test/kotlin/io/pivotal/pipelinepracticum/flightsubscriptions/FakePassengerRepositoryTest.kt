package io.pivotal.pipelinepracticum.flightsubscriptions

class FakePassengerRepositoryTest: PassengerRepositoryContract() {
    val repo = FakePassengerRepository()

    override fun repository(): PassengerRepository {
        return repo
    }
}