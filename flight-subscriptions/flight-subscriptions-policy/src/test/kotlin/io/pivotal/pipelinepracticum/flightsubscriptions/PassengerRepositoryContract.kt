package io.pivotal.pipelinepracticum.flightsubscriptions

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate

abstract class PassengerRepositoryContract {
    abstract fun repository(): PassengerRepository

    @Test
    fun `saving and fetching passengers`() {
        val passenger = Passenger(
                name = "Chaalix",
                subscriptions = listOf(
                        Flight("SOME-FLIGHT", LocalDate.of(2018, 11, 5)),
                        Flight("SOME-OTHER-FLIGHT", LocalDate.of(2018, 11, 12))
                )
        )

        val savedPassenger = repository().savePassenger(passenger)

        val id = savedPassenger.id!!
        val fetchedPassenger = repository().findPassengerById(id)!!

        assertThat(fetchedPassenger).isEqualTo(passenger.copy(id = fetchedPassenger.id))
        assertThat(savedPassenger).isEqualTo(passenger.copy(id = savedPassenger.id))

        val otherSavedPassenger = repository().savePassenger(Passenger(name = "Tiglax", subscriptions = emptyList()))

        assertThat(repository().findAllPassengers()).containsExactlyInAnyOrder(savedPassenger, otherSavedPassenger)
    }

    @Test
    fun `updating a passenger`() {
        val originalPassenger = repository().savePassenger(Passenger(
                name = "Chaalix",
                subscriptions = listOf(
                        Flight("SOME-FLIGHT", LocalDate.of(2018, 11, 5)),
                        Flight("SOME-OTHER-FLIGHT", LocalDate.of(2018, 11, 12))
                )
        ))

        val updatedPassenger = repository().savePassenger(originalPassenger.copy(
                subscriptions = originalPassenger.subscriptions.plus(
                        Flight("NEW-FLIGHT", LocalDate.of(2019, 1, 30))
                )
        ))

        assertThat(updatedPassenger.id).isEqualTo(originalPassenger.id)

        assertThat(repository().findPassengerById(updatedPassenger.id!!)).isEqualTo(updatedPassenger)
    }

}