package io.pivotal.pipelinepracticum.flightsubscriptions

interface SubscribedFlightStatuses {
    fun forPassenger(passenger: Passenger): Map<Flight, String>
}
