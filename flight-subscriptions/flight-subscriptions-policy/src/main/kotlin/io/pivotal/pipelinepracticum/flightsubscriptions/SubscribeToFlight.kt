package io.pivotal.pipelinepracticum.flightsubscriptions

import java.time.LocalDate

class SubscribeToFlight(private val passengerRepository: PassengerRepository) {

    interface Outcome<T> {
        fun successfullySubscribed(updatedPassenger: Passenger): T
        fun noSuchPassenger(passengerId: String): T
    }

    fun <T> execute(flightNumber: String, date: LocalDate, passengerId: String, outcome: Outcome<T>): T {
        val passenger = passengerRepository.findPassengerById(passengerId)

        if(passenger == null) return outcome.noSuchPassenger(passengerId)

        val updatedPassenger = passengerRepository.savePassenger(passenger.copy(
                subscriptions = passenger.subscriptions.plus(Flight(flightNumber, date))
        ))

        return outcome.successfullySubscribed(updatedPassenger)
    }
}