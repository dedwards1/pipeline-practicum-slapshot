package io.pivotal.pipelinepracticum.flightsubscriptions.web

import io.pivotal.pipelinepracticum.flightsubscriptions.Passenger
import io.pivotal.pipelinepracticum.flightsubscriptions.PassengerRepository
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
class SignupController(val passengerRepository: PassengerRepository) {
    @GetMapping("/sign-up")
    fun newPassenger(): String {
        return "new-passenger"
    }

    @PostMapping("/sign-up")
    fun createPassenger(@RequestParam("passenger_name") name: String): String {
        passengerRepository.savePassenger(Passenger(name = name))
        return "index"
    }
}