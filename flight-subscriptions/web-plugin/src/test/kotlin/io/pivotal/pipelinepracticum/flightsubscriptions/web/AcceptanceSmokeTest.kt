package io.pivotal.pipelinepracticum.flightsubscriptions.web

import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import org.junit.experimental.categories.Category
import java.time.LocalDate

@Category(AcceptanceSmokeTest::class)
class AcceptanceSmokeTest : FlightSubscriptionsJourney() {
    override fun existingFlights() = mapOf(
            Flight("KS 999", LocalDate.of(2018, 12, 31)) to "On Time" )

    override fun baseURL() = "https://atka-air-gadoid-regeneracy.apps.pcfone.io/"
}