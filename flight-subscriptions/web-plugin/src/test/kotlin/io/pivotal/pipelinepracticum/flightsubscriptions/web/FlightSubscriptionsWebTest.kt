package io.pivotal.pipelinepracticum.flightsubscriptions.web

import io.pivotal.pipelinepracticum.flightsubscriptions.*
import org.junit.runner.RunWith
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import java.time.LocalDate

val flightData = mapOf(
        Flight("KS 244", LocalDate.of(2018, 11, 5)) to "Delayed",
        Flight("KS 244", LocalDate.of(2018, 11, 12)) to "On Time"
)

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [FlightSubscriptionsWebTestConfig::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("FlightSubscriptionsWebTest")
class FlightSubscriptionsWebTest : FlightSubscriptionsJourney() {
    override fun existingFlights() = flightData
    override fun baseURL() = "http://localhost:$port"

    @LocalServerPort
    private lateinit var port: String

}

@SpringBootApplication
@Profile("FlightSubscriptionsWebTest")
class FlightSubscriptionsWebTestConfig {

    @Bean
    fun passengerRepository() = FakePassengerRepository()

    @Bean
    fun subscribeToFlight(passengerRepository: PassengerRepository) = SubscribeToFlight(passengerRepository)

    @Bean
    fun subscribedFlightStatuses(): SubscribedFlightStatuses = object : SubscribedFlightStatuses {
        override fun forPassenger(passenger: Passenger): Map<Flight, String> = flightData
    }
}