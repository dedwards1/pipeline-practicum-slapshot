package io.pivotal.pipelinepracticum.atkaair

import io.pivotal.pipelinepracticum.flightstatus.FlightStatusLookup
import io.pivotal.pipelinepracticum.flightstatus.airtrafficcontrol.ATCBackedFlightStatusLookup
import io.pivotal.pipelinepracticum.flightsubscriptions.FakePassengerRepository
import io.pivotal.pipelinepracticum.flightsubscriptions.SubscribeToFlight
import io.pivotal.pipelinepracticum.flightsubscriptions.db.DbBackedPassengerRepository
import io.pivotal.pipelinepracticum.flightsubscriptions.flightstatus.FlightStatusBackedSubscriptionStatusAdapter
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.web.client.RestTemplate

@SpringBootApplication
@ComponentScan("io.pivotal.pipelinepracticum.flightsubscriptions.web")
@EnableJpaRepositories("io.pivotal.pipelinepracticum.flightsubscriptions.db")
@EntityScan("io.pivotal.pipelinepracticum.flightsubscriptions.db")
@Import(
        DbBackedPassengerRepository::class,
        SubscribeToFlight::class,
        FlightStatusBackedSubscriptionStatusAdapter::class
)
class PipelinePracticumMainApplication {
    @Bean
    fun atcBackedFlightStatusLookup(@Value("\${ATC_SERVICE_URL}") atcServiceUrl: String, restTemplate: RestTemplate): FlightStatusLookup {
        return ATCBackedFlightStatusLookup(atcServiceUrl, restTemplate)
    }

    @Bean
    fun restTemplate() = RestTemplate()
}

fun main(args: Array<String>) {
    runApplication<PipelinePracticumMainApplication>(*args)
}
